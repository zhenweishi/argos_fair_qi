"""
###############################
@author: Tianchen Luo, Maastro##
###############################

Decompress compressed dicom file in batch

To use this script:
    
    Move all compressed dicom files into a folder and set the path under main

    e.g: path = '/Users/luotianchen/Desktop/argos_fair_qi/test_compressed/'

    So, move all patients' dicom files under 'test_compressed' folder: 
    e.g: '/Users/luotianchen/Desktop/argos_fair_qi/test_compressed/patient1'
         '/Users/luotianchen/Desktop/argos_fair_qi/test_compressed/patient2'
         '/Users/luotianchen/Desktop/argos_fair_qi/test_compressed/patient100'
    
    This script will decompress all folders and create a new folder to store decompressed dicom files
"""




import gdcm
import sys
import os


debug = False
# create folder

def decompresse_dcm(input,output):
    
    reader = gdcm.ImageReader()
    reader.SetFileName(input)
    
    if debug: print('---',reader.Read())
    
    change = gdcm.ImageChangeTransferSyntax()
    change.SetTransferSyntax(gdcm.TransferSyntax(gdcm.TransferSyntax.ImplicitVRLittleEndian))
    change.SetInput(reader.GetImage())

    if debug: print('---',change.Change())
    
    writer = gdcm.ImageWriter()
    writer.SetFileName(output)
    writer.SetFile(reader.GetFile())
    writer.SetImage(change.GetOutput())

    if debug: print('---',writer.Write())

 
def get_list(file_dir):
    val = []

    for root, dirs, files in os.walk(file_dir):
        val = files

    return val

def decompress_method(file_list, input_folder, output_folder):

    for i in range(0,len(file_list)):
        output = output_folder+str(file_list[i])

        if debug: print('----',input_folder+str(file_list[i]))

        decompresse_dcm(input_folder+str(file_list[i]),output)


def batch_decompressed(file_dir):
    # Collect path
    roots = []

    for root, dirs, files in os.walk(file_dir):
    
        roots.append(root)    

    # Collect folder name
    folder_name = []

    for root in roots:
        temp_list = root.split( '/')
        folder_name.append(temp_list[-1])
    
    # Delete the root fold path
    folder_name.pop(0)
    
    # Create new folders for decompressed dicom files
    for name in folder_name:
        Decompressed_folder = path+str(name)+'_decompressed'
        try:
            os.mkdir(Decompressed_folder)
        except:
            pass
    
    root_path = roots.pop(0)

    for i in range(0,len(folder_name)):
        #print('input list is: ',roots[i])
        #print('output list is: ' ,root_path+folder_name[i]+'_decompressed/')
        compressed_list = get_list(roots[i])
        print(compressed_list)
        decompress_method(compressed_list,roots[i]+'/',root_path+folder_name[i]+'_decompressed/')

    print(len(folder_name))
    print(len(roots))


    
    
    



if __name__ == "__main__":
    debug = False

    '''
    # Set the compressed CT folder path here
    # Put all compressed CT in this path, this script will decompress all files and create new decompressed fold
    # For each patient
    '''
    path = '/Users/luotianchen/Desktop/argos_fair_qi/test_compressed/'

    batch_decompressed(path)